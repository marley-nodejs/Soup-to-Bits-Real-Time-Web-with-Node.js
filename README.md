# Soup-to-Bits-Real-Time-Web-with-Node.js
CodeSchool - Soup to Bits Real-Time Web with Node.js

___

https://github.com/codeschool/nodejs-soup-to-bits-pub-sub-server  
https://github.com/codeschool/nodejs-soup-to-bits-web-server  
https://gist.github.com/TJkrusinski/10735569  
___


REDIS SERVER INSTALLATION ON (CENTOS 6.X)

    yum install tcl

    cd /tmp/
    wget http://download.redis.io/releases/redis-2.8.19.tar.gz
    tar xzf redis-2.8.19.tar.gz
    cd redis-2.8.19
    make
    make test
    make install
    cd utils
    ./install_server.sh
    service redis_6379 restart

===

    cd soup/pubsub
    npm init
    npm install --save express@3.x.x axon redis underscore


    nodemon app.js
    curl -X POST http://localhost:8080


===

    cd soup/pubsub
    nodemon app.js


    curl -X POST http://localhost:8080 -H "content-type: application/json" -d  '[{"badge_id": "foo bar badge"}]'

удаляем записи из базы

    redis-cli
    > LRANGE badges 0 -1
    > LPOP badges
    > LPOP badges

time: 42:44

===

    cd soup/pubsub
    nodemon app.js


несколько раз делаем запрос:

    curl -X POST http://localhost:8080 -H "content-type: application/json" -d  '[{"badge_id": "foo bar badge"}]'

в базе должны появиться записи вида:

    $ redis-cli
    127.0.0.1:6379> LRANGE badges 0 -1
        1) "{\"badge_id\":\"foo bar badge\"}"
        2) "{\"badge_id\":\"foo bar badge\"}"
        3) "{\"badge_id\":\"foo bar badge\"}"
        4) "{\"badge_id\":\"foo bar badge\"}"
        5) "{\"badge_id\":\"foo bar badge\"}"
        6) "{\"badge_id\":\"foo bar badge\"}"
        7) "{\"badge_id\":\"foo bar badge\"}"
        8) "{\"badge_id\":\"foo bar badge\"}"


потом

    $ curl http://localhost:8080/badges
    [
    {
    "badge_id": "foo bar badge"
    },
    {
    "badge_id": "foo bar badge"
    },
    {
    "badge_id": "foo bar badge"
    },
    {
    "badge_id": "foo bar badge"
    },
    {
    "badge_id": "foo bar badge"
    },
    {
    "badge_id": "foo bar badge"
    },
    {
    "badge_id": "foo bar badge"
    },
    {
    "badge_id": "foo bar badge"
    }

time: 1:08:08

===


    cd soup/webserver
    npm init
    npm install --save express@3.x.x axon socket.io request

    mkdir lib
    mkdir models
    mkdir public

===

Нужно стартовать 3 приложения.  
Подключиться:  
http://localhost:3000/  

Картинок у меня не было, я не стал их искать, качать с сайта и т.д.
